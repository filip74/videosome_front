import { Injectable } from '@angular/core';

import { FetchService } from './fetch.service';
import { Room, RoomState } from '../models/room.models';

@Injectable()
export class RoomService {
  constructor(
    private fetchService: FetchService,
  ) { }

  createRoom(payload: any) {
    return this.fetchService.post('/rooms/', payload);
  }

  getRooms(): Promise<Room[]> {
    return this.fetchService.get('/rooms/')
      .then(response => response.json());
  }

  getRoomState(roomId: string) {
    return this.fetchService.get(`/room_state/${roomId}/`)
      .then(response => response.json());
  }
}
