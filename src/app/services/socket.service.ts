import { Injectable } from '@angular/core';

import { WebSocketBridge } from '../websocketbridge';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../environments/environment';
import { AuthService } from './auth.service';
import { Payload, YoutubePlayerEvent, RoomState } from '../models/socket.models';

@Injectable()
export class SocketService {
  private webSocketBridge: WebSocketBridge = new WebSocketBridge();
  private _messages$: Subject<Payload> = new Subject();
  private _playerEvents$: Subject<Payload> = new Subject();
  private chatStream: string = 'chat';
  private roomStream: string = 'room';
  private connected = false;

  messages$: Observable<Payload> = this._messages$.asObservable();
  playerEvents$: Observable<Payload> = this._playerEvents$.asObservable();

  constructor(
    private authService: AuthService,
  ) {
    this.webSocketBridge.demultiplex(this.chatStream, (payload: object, stream: string) => {
      this._messages$.next(payload as Payload);
    });
    this.webSocketBridge.demultiplex(this.roomStream, (payload: object, stream: string) => {
      this._playerEvents$.next(payload as Payload);
    });
  }

  connect(roomId: string) {
    if (!this.connected) {
      this.webSocketBridge.connect(`${environment.wsUrl}${roomId}/?token=${this.authService.getToken()}`);
      this.webSocketBridge.listen();
      this.connected = true;
    }
  }

  sendChatMessage(message: object) {
    this.webSocketBridge.stream(this.chatStream).send({message});
  }

  close() {
    if (this.connected) {
      this.webSocketBridge.close();
    }
    this.connected = false;
  }

  sendPlayerPause() {
    this.webSocketBridge.stream(this.roomStream).send({
      event: 'paused',
    });
  }

  sendPlayerPlay() {
    this.webSocketBridge.stream(this.roomStream).send({
      event: 'playing',
    });
  }

  sendPlayerTimeUpdate(timestamp: number) {
    this.webSocketBridge.stream(this.roomStream).send({
      event: 'timeupdate',
      payload: {
        timestamp,
      }
    });
  }

  sendRoomStateUpdate(state: RoomState) {
    this.webSocketBridge.stream(this.roomStream).send({
      event: 'UPDATE_ROOM_STATE',
      ...state,  // TODO: send state under `payload` key so it's consistent with other messages
    });
  }

  sendChangeVideo(videoUrl: string) {
    this.webSocketBridge.stream(this.roomStream).send({
      event: 'CHANGE_VIDEO',
      payload: {
        video_url: videoUrl,
      },
    });
  }
}
