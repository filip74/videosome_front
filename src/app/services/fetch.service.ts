import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { environment } from '../../environments/environment';
import { TOKEN_STORAGE_KEY } from './auth.service'

@Injectable()
export class FetchService {
  API_URL: string = environment.apiUrl;
  
  constructor(
    private http: Http,
  ) { }

  prepareHeaders(extraHeaders: { [key: string]: string } = {}): Headers {
    const headers: { [key: string]: string } = {
      'Content-Type': 'application/json',
      ...extraHeaders,
    };

    let ret: Headers = new Headers();
    for (let key in headers) {
      let value = headers[key];
      ret.set(key, value);
    }

    let token: string = localStorage.getItem(TOKEN_STORAGE_KEY);
    if (token !== null) {
      ret.set('Authorization', `Token ${token}`);
    }

    return ret
  }

  get(url: string, payload: any = {}): Promise<Response> {
    let searchParams: URLSearchParams = new URLSearchParams();
    for (let key in payload) {
      let value = payload[key];
      searchParams.set(key, value);
    }
    return this.http.get(
      `${this.API_URL}${url}`, {
      search: searchParams,
      headers: this.prepareHeaders(),
    }).toPromise();
  }

  post(url: string, payload: any = {}): Promise<Response> {
    return this.http.post(`${this.API_URL}${url}`, JSON.stringify(payload), {
      headers: this.prepareHeaders(),
    }).toPromise();
  }

  put(url: string, payload: any = {}): Promise<Response> {
    return this.http.put(`${this.API_URL}${url}`, JSON.stringify(payload), {
      headers: this.prepareHeaders(),
    }).toPromise();
  }

  patch(url: string, payload: any = {}): Promise<Response> {
    return this.http.patch(`${this.API_URL}${url}`, JSON.stringify(payload), {
      headers: this.prepareHeaders(),
    }).toPromise();
  }
}