import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { FetchService } from './fetch.service';
import { User } from '../models/auth.models';

export const TOKEN_STORAGE_KEY: string = '_TOKEN';

@Injectable()
export class AuthService {
  private _currentUser$: BehaviorSubject<User> = new BehaviorSubject(null);
  currentUser$: Observable<User> = this._currentUser$.asObservable();

  constructor(
    private fetchService: FetchService,
  ) { }

  logIn(credentials: any): Promise<Response> {
    return this.fetchService.post('/login/', credentials)
      .then(response => response.json())
      .then(data => {
        this._currentUser$.next(data.user)
        this.saveToken(data.authToken);
        return data;
      });
  }

  logOut(): void {
    this._currentUser$.next(null);
    this.deleteToken();
  }

  signUp(credentials: any): Promise<Response> {
    return this.fetchService.post('/sign_up/', credentials)
      .then(response => response.json())
  }

  saveToken(token: string) {
    localStorage.setItem(TOKEN_STORAGE_KEY, token);
  }

  deleteToken() {
    localStorage.removeItem(TOKEN_STORAGE_KEY);
  }

  getToken(): string {
    return localStorage.getItem(TOKEN_STORAGE_KEY);
  }

  checkStoredToken() {
    this.fetchService.get('/me/')
      .then(response => response.json())
      .then(data => {
        this._currentUser$.next(data.user);
        return data;
      })
      .catch(() => {
        this.deleteToken();
        this._currentUser$.next(null);
      });
  }

  getCurrentUserInfo(): Promise<Response> {
    return this.fetchService.get('/me/');
  }
}
