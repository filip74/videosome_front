// TODO: update to TypeScript 2.4.0+ which supports string values for Enum members
export type PayloadStatus = "ERROR" | "SUCCESS";
export type PayloadType = "CONNECT" | "RECEIVE" | "DISCONNECT";

/**
 * Base WebSocket response model
 * Every response should contain at least those two fields
 */
export interface Payload {
  status: PayloadStatus;
  type: PayloadType;
}

export interface Author {
  username: string;
  id: number;
}

/**
 * Model for chat message
 */
export interface Message extends Payload {
  author: Author;
  message: string;
  timestamp: string;
}

export interface YoutubePlayerEvent extends Payload {
  event: YoutubePlayerEventType;
  payload?: any;
}

export interface RoomState {
  time: number;
  state: YoutubePlayerState;
  video_id: string;
}