export interface Room {
  name: string;
  title: string;
  owner: number;  // id of owner
}

export interface RoomState {
  videoId: string;
  time: number;
  state: YoutubePlayerState;
  ownerId: number;
}
