import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ToasterService } from 'angular2-toaster';

import { AuthService } from '../services/auth.service';

@Component({
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css'],
})
export class LogInComponent {
  logInForm: FormGroup;
  errors: any = {};
  isSending: boolean = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toasterService: ToasterService,
    private router: Router,
  ) {
    this.logInForm = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onLogInFormSubmit(formData: any): void {
    this.errors = {};
    this.isSending = true;
    this.authService.logIn(formData)
      .then(() => {
        this.isSending = false;
        this.toasterService.pop('success', 'Successfully logged in!');
        this.router.navigateByUrl('/');
      })
      .catch(errors => {
        this.isSending = false;
        this.errors = errors.json()
      });
  }
}