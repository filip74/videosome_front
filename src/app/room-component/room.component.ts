import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/interval';

import YTPlayer from 'yt-player';

import { SocketService } from '../services/socket.service';
import { RoomService } from '../services/room.service';
import { Payload, YoutubePlayerEvent } from '../models/socket.models';
import { RoomState } from '../models/room.models';

@Component({
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
})
export class RoomComponent implements OnInit, OnDestroy {
  roomName: string;
  player: YoutubePlayer;
  lastTimestamp: number;
  changeVideoForm: FormGroup;
  private stateUpdateSubscription: Subscription;

  constructor(
    private socketService: SocketService,
    private activatedRoute: ActivatedRoute,
    private roomService: RoomService,
    private formBuilder: FormBuilder,
  ) {
    this.roomName = activatedRoute.snapshot.paramMap.get('roomName');
    
    this.changeVideoForm = formBuilder.group({
      videoUrl: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.socketService.connect(this.roomName);

    this.player = new YTPlayer('#player', {
      width: '100%',
      height: '100%',
    });

    this.roomService.getRoomState(this.roomName)
      .then((roomState: RoomState) => {
        this.player.load(roomState.videoId);
        this.lastTimestamp = roomState.time;
        this.player.seek(roomState.time);
        if (roomState.state === 'playing') {
          this.player.play();
        } else {
          this.player.pause();
        }
      })

    this.player.on('paused', () => {
      this.socketService.sendPlayerPause();
    });
    this.player.on('playing', () => {
      this.socketService.sendPlayerPlay();
    });
    this.player.on('timeupdate', (newTimestamp) => {
      // send timestamp update only if it has changed by more than 1.5 seconds
      // this event is fired when player.getCurrentTime() changes, so it fires every x seconds
      // if video is running. by checking the difference between last two timestamps, we can more or less
      // decide whether the event was caused by user manually changing the timestamp
      if (Math.abs(newTimestamp - this.lastTimestamp) > 1.5) {
        this.socketService.sendPlayerTimeUpdate(newTimestamp);
      }
      this.lastTimestamp = newTimestamp;
    });

    this.socketService.playerEvents$.subscribe((payload: Payload) => {
      if (payload.status === 'SUCCESS' && payload.type === 'RECEIVE') {
        let event = payload as YoutubePlayerEvent;
        switch (event.event) {
          case 'playing': {
            this.player.play();
            break;
          }
          
          case 'paused': {
            this.player.pause();
            break;
          }

          case 'timeupdate': {
            this.player.seek(event.payload.timestamp);
            break;
          }

          case 'CHANGE_VIDEO': {
            this.player.load(event.payload.video_id);
            break;
          }
        }
      }
    });

    // update video's state in backend every 2 seconds
    this.stateUpdateSubscription = Observable.interval(2000).subscribe(() => {
      this.socketService.sendRoomStateUpdate({
        time: Math.ceil(this.player.getCurrentTime()),
        state: this.player.getState(),
        video_id: this.player.videoId,
      });
    });
  }

  synchronizeVideo() {
    // get video state and set client's video state to match the response from backend
    this.roomService.getRoomState(this.roomName)
      .then((roomState: RoomState) => {
        // reload video only if needed
        if (this.player.videoId !== roomState.videoId) {
          this.player.load(roomState.videoId);
        }

        this.player.seek(roomState.time);

        if (roomState.state === 'playing') {
          this.player.play();
        } else {
          this.player.pause();
        }
      });
  }

  onChangeVideoFormSubmit(formValue) {
    this.socketService.sendChangeVideo(formValue.videoUrl);
  }

  ngOnDestroy() {
    this.socketService.close();
    this.stateUpdateSubscription.unsubscribe();
  }
}