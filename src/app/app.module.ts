import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Route } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToasterModule, ToasterService } from 'angular2-toaster';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header-component/header.component';
import { DashboardComponent } from './dashboard-component/dashboard.component';
import { LogInComponent } from './log-in-component/log-in.component';
import { SignUpComponent } from './sign-up-component/sign-up.component';
import { ChatComponent } from './chat-component/chat.component';
import { RoomComponent } from './room-component/room.component';
import { CreateRoomComponent } from './create-room-component/create-room.component';

import { FetchService } from './services/fetch.service';
import { AuthService } from './services/auth.service';
import { SocketService } from './services/socket.service';
import { RoomService } from './services/room.service';

import { AuthGuard } from './guards/auth.guard';

import { CapitalizePipe } from './pipes';

const routes: Route[] = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'log-in',
    component: LogInComponent,
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
  },
  {
    path: 'room/create',
    component: CreateRoomComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'room/:roomName',
    component: RoomComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    LogInComponent,
    SignUpComponent,
    ChatComponent,
    RoomComponent,
    CreateRoomComponent,

    CapitalizePipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    ToasterModule,
    BrowserAnimationsModule,
  ],
  providers: [
    FetchService,
    AuthService,
    SocketService,
    RoomService,
    ToasterService,
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
