import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthService } from '../services/auth.service';
import { SocketService } from '../services/socket.service';
import { Payload, Message } from '../models/socket.models';

@Component({
  templateUrl: 'chat.component.html',
  styleUrls: ['chat.component.css'],
  selector: 'app-chat',
})
export class ChatComponent implements OnInit, OnDestroy{
  chatMessageForm: FormGroup;
  roomId: string;
  messages: Message[] = [];

  constructor(
    private socketService: SocketService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private elementRef: ElementRef,
  ) {
    this.chatMessageForm = formBuilder.group({
      message: ['', Validators.required],
    });

    this.roomId = activatedRoute.snapshot.paramMap.get('roomId');
  }

  onChatMessageFormSubmit(formValue) {
    (this.chatMessageForm.get('message') as FormControl).setValue('');
    this.socketService.sendChatMessage(formValue.message);
  }

  private handleChatMessageReceive(message: Message) {
    this.messages.push(message);
    let chatContainer: Element = this.elementRef.nativeElement.querySelector('.chat__messages-container');
    chatContainer.scrollTop = chatContainer.scrollHeight;
  }

  ngOnInit() {
    this.socketService.connect(this.roomId);
    this.socketService.messages$.subscribe((payload: Payload) => {
      if (payload.status == "SUCCESS" && payload.type == "RECEIVE") {
        this.handleChatMessageReceive(payload as Message);
      }
    });
  }

  ngOnDestroy() {
    this.socketService.close();
  }
}
