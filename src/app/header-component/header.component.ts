import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { ToasterService } from 'angular2-toaster';

import { AuthService } from '../services/auth.service';
import { User } from '../models/auth.models';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css',],
})
export class HeaderComponent {
  user$: Observable<User>;

  constructor(
    private authService: AuthService,
    private toasterService: ToasterService,
    private router: Router,
  ) {
    this.user$ = authService.currentUser$;
  }

  logOut(): void {
    this.authService.logOut();
    this.toasterService.pop('success', 'You have been logged out.');
    this.router.navigateByUrl('/');
  }
}