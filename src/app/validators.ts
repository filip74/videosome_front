import { ValidatorFn, AbstractControl } from '@angular/forms';

export function regexValidator(pattern: RegExp, errorKey: string = 'nomatch'): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const value = control.value;
    return pattern.test(value) ? null : {[errorKey]: (value)} ;
  }
}

export const alphanumericValidator: ValidatorFn = regexValidator(/^[a-zA-Z0-9]+$/, 'nonalphanumeric');
