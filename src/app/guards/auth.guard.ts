import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/last';
import 'rxjs/add/operator/map';

import { ToasterService } from 'angular2-toaster';

import { AuthService } from '../services/auth.service';
import { User } from '../models/auth.models';

@Injectable()
export class AuthGuard implements CanActivate {
  private didFetchCurrentUser: boolean = false;
  private isLoggedIn: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private toasterService: ToasterService,
  ) {
    this.authService.currentUser$.subscribe((user: User) => {
      this.isLoggedIn = user !== null;
    });
  }
  
  canActivate(): Promise<boolean> | Observable<boolean> | boolean {
    if (this.didFetchCurrentUser) {
      if (!this.isLoggedIn) {
        this.redirectToLogin();
      }
      return this.isLoggedIn;
    }

    return this.authService.getCurrentUserInfo()
      .then(() => {
        this.didFetchCurrentUser = true;
        return true;
      })
      .catch(() => {
        this.didFetchCurrentUser = true;
        this.redirectToLogin();
        return false;
      })
  }

  redirectToLogin() {
    this.router.navigateByUrl('/log-in');
    this.toasterService.pop(
      'error',
      'Unauthorized',
      'Only logged in users can view this page.'
    );
  }
}
