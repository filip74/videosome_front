import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { RoomService } from '../services/room.service';
import { Room } from '../models/room.models';
import { alphanumericValidator } from '../validators';

@Component({
  templateUrl: './create-room.component.html',
})
export class CreateRoomComponent {
  createRoomForm: FormGroup;
  sending: boolean = false;
  errors: object = null;
  
  constructor(
    private formBuilder: FormBuilder,
    private roomService: RoomService,
    private router: Router,
  ) {
    // TODO: fetch validation rules from backend?
    this.createRoomForm = formBuilder.group({
      name: ['', [
        Validators.minLength(3),
        Validators.maxLength(10),
        Validators.required,
        alphanumericValidator,
      ]],
      title: ['', Validators.maxLength(50)],
      videoUrl: ['', Validators.required],  // TODO: add URL validator
    });
  }

  onCreateRoomFormSubmit(formVal: object) {
    this.sending = true;

    this.roomService.createRoom(formVal)
      .then(response => response.json())
      .then((room: Room) => {
        this.router.navigate([`/room/${room.name}`]);
      })
      .catch(error => {
        this.errors = error.json();
        this.sending = false;
      });
  }
}
