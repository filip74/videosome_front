/**
 * This is pretty much a copy-paste from django-channels, with a little twist:
 * - Added a bit of TypeScript love
 * - Changed RecoonectingWebSocket with native WebSocket, since the former seems to be an abandoned project
 *   and it does not really work well when it comes to terminating the connection - it still tries to revive it,
 *   what caused a new socket to open each time user navigated out and back to the room (or any component that
 *   initialized a connection).
 */
export class WebSocketBridge {
  private socket: WebSocket;
  private streams: object;
  private default_cb: any;
  private options: object;

  constructor() {
    this.socket = null;
    this.streams = {};
    this.default_cb = null;
  }

  connect(url: string) {
    let _url;
    // Use wss:// if running on https://
    const scheme = window.location.protocol === 'https:' ? 'wss' : 'ws';
    const base_url = `${scheme}://${window.location.host}`;
    if (url === undefined) {
      _url = base_url;
    } else {
      // Support relative URLs
      if (url[0] == '/') {
        _url = `${base_url}${url}`;
      } else {
        _url = url;
      }
    }
    this.socket = new WebSocket(_url);
  }

  listen(cb?: any) {
    this.default_cb = cb;
    this.socket.onmessage = (event) => {
      const msg = JSON.parse(event.data);
      let action;
      let stream;

      if (msg.stream !== undefined) {
        action = msg.payload;
        stream = msg.stream;
        const stream_cb = this.streams[stream];
        stream_cb ? stream_cb(action, stream) : null;
      } else {
        action = msg;
        stream = null;
        this.default_cb ? this.default_cb(action, stream) : null;
      }
    };
  }

  demultiplex(stream, cb) {
    this.streams[stream] = cb;
  }

  send(msg) {
    this.socket.send(JSON.stringify(msg));
  }

  stream(stream) {
    return {
      send: (action) => {
        const msg = {
          stream,
          payload: action
        }
        this.socket.send(JSON.stringify(msg));
      }
    }
  }

  close() {
    this.socket.close();
  }

}