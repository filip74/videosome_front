import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { ToasterService } from 'angular2-toaster';

import { AuthService } from '../services/auth.service';

@Component({
  templateUrl: './sign-up.component.html',
})
export class SignUpComponent {
  signUpForm: FormGroup;
  errors: any = {};
  isSending: boolean = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toasterService: ToasterService,
    private router: Router,
  ) {
    this.signUpForm = fb.group({
      username: ['', Validators.required],
      password: ['', [
        Validators.required,
        Validators.minLength(3),
      ]],
      passwordConfirmation: ['', Validators.required],
    }, {
      validator: this.passwordIdentityValidator,
    });
  }

  onSignUpFormSubmit(formVal: any) {
    this.isSending = true;
    this.authService.signUp(formVal)
      .then(() => {
        this.isSending = false;
        this.toasterService.pop(
          'success',
          'Your account has been created!',
          'You can now log in onto your new account.'
        );
        this.router.navigateByUrl('/log-in');
      })
      .catch(e => {
        this.errors = e.json();
        this.isSending = false;
      });
  }

  passwordIdentityValidator(formGroup: FormGroup): {[key: string]: any} {
    if (formGroup.get('password').value !== formGroup.get('passwordConfirmation').value) {
      return {
        passwordIdentity: true,
      };
    }
    return null;
  }
}