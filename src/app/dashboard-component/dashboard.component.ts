import { Component } from '@angular/core';

import { RoomService } from '../services/room.service';
import { Room } from '../models/room.models';

@Component({
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {
  rooms: Room[];
  isLoading: boolean = true;
  error: boolean = false;

  constructor(
    private roomService: RoomService,
  ) {
    roomService.getRooms()
      .then((rooms: Room[]) => {
        this.isLoading = false;
        this.rooms = rooms;
      })
      .catch(() => {
        this.error = true;
      });
  }

  getRoomPath(room: Room) {
    return `/room/${room.name}`;
  }
}
