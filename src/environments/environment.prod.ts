export const environment = {
  production: false,
  apiUrl: 'http://videosome.hawi.pl/api/v1',
  wsUrl: 'ws://videosome.hawi.pl/stream/',
  streams: {
    chat: 'chat',
  },
};
