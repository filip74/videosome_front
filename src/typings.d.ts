/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare class YoutubePlayerOptions {
  width: number;
  height: number;
  autoplay: boolean;
  captions: boolean;
  controls: boolean;
  keyboard: boolean;
  fullscreen: boolean;
  annotations: boolean;
  modestBranding: boolean;
  related: boolean;
  info: boolean;
  timeupdateFrequency: number;
}

declare type YoutubePlayerEventType =
  'error' |
  'unplayable' |
  'timeupdate' |
  'unstarted' |
  'ended' |
  'playing' |
  'paused' |
  'buffering' |
  'cued' |
  'playbackQualityChange' |
  'playbackRateChange' |
  'CHANGE_VIDEO';

declare type YoutubePlayerState =
  'unstarted' |
  'ended' |
  'playing' |
  'paused' |
  'buffering' |
  'cued';

declare class YoutubePlayer {
  constructor(element: string, options?: YoutubePlayerOptions);

  load(videoId: string, autoplay?: boolean);
  play();
  pause();
  stop();
  seek(seconds: number);
  setVolume(volume: number);
  setPlaybackRate(rate: number);
  getVolume(): number;
  getPlaybackRate(): number;
  getAvailablePlaybackRates(): number[];
  getDuration(): number;
  getProgress(): number;
  getState(): YoutubePlayerState;
  getCurrentTime(): number;
  destroy();
  destroyed(): boolean;
  videoId: string;
  on(event: YoutubePlayerEventType, callback: (...args: any[]) => void);
}
